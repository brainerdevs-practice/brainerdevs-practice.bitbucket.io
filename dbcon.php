<!-- hi there my name is shawan -->
<?php

// 1. DB Connection Stablish

$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "phpprac";
$con = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

// Test Connection

if (mysqli_connect_errno()){

	die("Database Connection Failed ".mysqli_connect_error()."(".mysqli_connect_errno().")");
}

?>

<?php

// 2. Perform DBQuery

$query = "SELECT * FROM subjects WHERE visible = 1";

$result = mysqli_query($con, $query);

if(!$result){
	die("Query Failed ");
}

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>    
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- ADDITIONAL STYLESHEET HERE -->
	<title>Database</title>
</head>
<body>
	<!-- ALL OF YOUR SITE CODE HERE -->
	<div class="container mt-4">
		<h2>Database Connection & Query Test</h2>

	<h4>Select Query</h4>
	<p>Update On Click</p>
	
<ul>
	<?php 

	// 3. User The Data

		while ($subject = mysqli_fetch_assoc($result)) {

			?>

			<li><a href="dbcon_update.php?id=<?php echo urlencode($subject['id']) ?>"><?php echo $subject['menu_name']." (".$subject['id'].")" ?></a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="dbcon_delete.php?id=<?php echo urlencode($subject['id']) ?>" onclick="alert('Are You Sure?')">Delete</a> </li>

			<?php 
		}
	 ?>

</ul>
<a href='dbcon_insert.php'>Insert New DB</a>
<?php 
// Release The Data
mysqli_free_result($result);

 ?>

	</div>
	<!-- ALL OF YOUR SITE CODE HERE -->    
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<!-- ADDITIONAL JS HERE -->
</body>
</html>

<?php 
// Close the connection

mysqli_close($con);

 ?>