
<?php

// 1. DB Connection Stablish

$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "phpprac";
$con = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

// Test Connection

if (mysqli_connect_errno()){

	die("Database Connection Failed ".mysqli_connect_error()."(".mysqli_connect_errno().")");
}

?>

<?php

$id = $_GET['id'];
$menu_name = "Subject Three";
$position = 3;
$visible = 0;

// 2. Perform DBQuery

$query = "UPDATE subjects SET ";

$query .= "menu_name ='{$menu_name}', ";

$query .= "position = {$position}, ";

$query .= "visible = {$visible} ";

$query .= "WHERE id = {$id}";

$result = mysqli_query($con, $query);

if($result && mysqli_affected_rows($con)>0){
	echo "Success <br> <a href='dbcon.php'>See List</a>";
}

else{
	die("Query Failed".mysqli_error($con));
	
}

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>    
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- ADDITIONAL STYLESHEET HERE -->
	<title>Database Update</title>
</head>
<body>
	<!-- ALL OF YOUR SITE CODE HERE -->
	<div class="container mt-4">
		<h2>Database Connection & Query Test</h2>

	<h4>Update Query</h4>
	




	</div>
	<!-- ALL OF YOUR SITE CODE HERE -->    
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<!-- ADDITIONAL JS HERE -->
</body>
</html>

<?php 
// Close the connection

mysqli_close($con);

 ?>