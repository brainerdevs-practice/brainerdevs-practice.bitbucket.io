

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- ADDITIONAL STYLESHEET HERE -->
	<title>Tables</title>
</head>
<body>
	<!-- ALL OF YOUR SITE CODE HERE -->
	<div class="container mt-4">
		<h4>Tables</h4>

		<form action="index.php" method="post">
			<div class="form-group">
				<label for="exampleInputEmail1">Enter First Value</label>
				<input type="number" class="form-control" name="value1" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="First Value">
			</div>
			<!-- <div class="form-group">
				<label for="exampleInputPassword1">Enter Second Value</label>
				<input type="number" class="form-control" name="value2" id="exampleInputPassword1" placeholder="Second Value">
			</div> -->
			<!-- <div class="form-group form-check">
				<input type="radio" class="form-check-input" name="cal" value="1" id="exampleCheck1">
				<label class="form-check-label" for="exampleCheck1">Add</label>
				<br>
				<input type="radio" class="form-check-input" name="cal" value="2" id="exampleCheck1">
				<label class="form-check-label" for="exampleCheck1">Subtract</label>
				<br>
				<input type="radio" class="form-check-input" name="cal" value="3" id="exampleCheck1">
				<label class="form-check-label" for="exampleCheck1">Multiplication</label>
				<br>
				<input type="radio" class="form-check-input" name="cal" value="4" id="exampleCheck1">
				<label class="form-check-label" for="exampleCheck1">Division</label>
			</div> -->
			<input type="submit" name="submit" value="Submit" class="btn btn-primary">
		</form>
<hr>
		<h3>Result</h3>
		<p><?php
		// echo "The Reasult Is: {$sum}";
		?></p>
		
<?php include('process.php')?>


	</div>
	<!-- ALL OF YOUR SITE CODE HERE -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<!-- ADDITIONAL JS HERE -->
</body>
</html>

