<?php

function calInput($a,$b,$cal)
{
	if($cal == 1){
		$c = $a + $b;
		return $c;
	}
	elseif ($cal == 2) {
		$c = $a - $b;
		return $c;
	}
	elseif ($cal == 3) {
		$c = $a * $b;
		return $c;
	}
	elseif ($cal == 4) {
		$c = $a / $b;
		return round($c,2);
	}
	else {
		return "Invalid Input";
	}
}

function formulsquare($a,$b){
	$c = pow($a, 2) + 2*$a*$b + pow($b, 2);
	return $c;
}

function tables($a){
	for($i = 1 ; $i<=$a; $i++){
	for($j = 1 ; $j<=10; $j++){

echo "{$i} * {$j} = ".$i*$j."<br>";
}
echo "<br>";
}

}

 ?>